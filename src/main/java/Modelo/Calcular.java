package Modelo;

public class Calcular {

    /**
     * @return the capital
     */
    public double getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(double capital) {
        this.capital = capital;
    }

    /**
     * @return the taza
     */
    public double getTaza() {
        return taza;
    }

    /**
     * @param taza the taza to set
     */
    public void setTaza(double taza) {
        this.taza = taza;
    }

    /**
     * @return the años
     */
    public double getAnos() {
        return anos;
    }

    /**
     * @param años the años to set
     */
    public void setAnos(double anos) {
        this.anos = anos;
    }
    private double capital;
    private double taza;
    private double anos;
    private double interessimple;
    
    
    public Calcular(){
        this.capital=0;
        this.taza=0;
        this.anos=0;
        this.interessimple=0;
        
    
        
    }
        public void interessimple(){
            this.setInteressimple(this.capital * (this.taza/100)* this.anos);
        
    }

    /**
     * @return the interessimple
     */
    public double getInteressimple() {
        return interessimple;
    }

    /**
     * @param interessimple the interessimple to set
     */
    public void setInteressimple(double interessimple) {
        this.interessimple = interessimple;
    }
            
    
}
